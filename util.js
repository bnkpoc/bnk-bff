global.__isDebugMode = true;		//console debug message 표시 여부

var util = {};

util.get_date = function(set_month) {
	var d = new Date();
	var lastDayofLastMonth = ( new Date( d.getYear(), d.getMonth(), 0) ).getDate();
	if(d.getDate() > lastDayofLastMonth) {
		d.setDate(lastDayofLastMonth);
	}
	
	var month = d.getMonth() - set_month;
	d.setMonth(month);

	var yyyy = d.getFullYear();
	var mm = ("0"+(d.getMonth()+1)).slice("-2"); //January is 0!
	var dd = ("0"+(d.getDate())).slice("-2");
	
	return (""+yyyy+mm+dd);
}

var d = new Date();
var monthOfYear = d.getMonth();
d.setMonth(monthOfYear - 1);

util.log = function(msg) {
	if(__isDebugMode) console.log(msg);
}

util.userData = {
	username: "",
	name: "",
	email: ""
}

util.uridecode = function(msg)
{
	var result = decodeURIComponent(msg.replace(/\+/g, ' '));

	return result;
}


module.exports = util;