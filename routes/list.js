/*jshint esversion: 6 */

//---- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
var querystring = require('querystring');
const util = require(__BASEDIR + '/util');
//---------------

//---- 기본 library 셋팅o
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//----------

//--- 전계좌 조회 화면 표시
router.get("/list", (req, res) => {
	util.log("전계좌조회 화면");

	let token = req.cookies[__ACCESS_TOKEN_NAME];
	let _headers = {};
	util.log("request token => " + token);
	_headers[__ACCESS_TOKEN_NAME] = token;
	_headers['cookie'] = req.cookies['ck-auth'].join('; ');
	_headers['Content-Type'] = 'application/json;charset=UTF-8';
	
	console.log('Cookies: ', req.cookies);
	console.log('token: ', token);
	
	axios.defaults.withCredentials = true;

	//결과데이터
	var data = {};

	var url = __LIST_API_URI+'/bnk/list';
	console.log('######### URL: ', url);

	var param = {
		'ib20_cur_mnu': 'MWPCMN0200CMN10',
		'jwt_token': _headers[__ACCESS_TOKEN_NAME],
		'cookie': _headers['cookie']
	};

	axios.post(url, 
		param,
		{
			headers: _headers
		}
	)
	.then((ret) => {
		if(ret.status == 200) {

			var ret_common_ = ret.data._msg_._common_;
			var ret_body_ = ret.data._msg_._body_;

			data.resCode = ret_common_['resCode'];
			data.customerMessage = ret_common_['error.customer.message'];

			if(ret_common_['resCode']=='00')
			{

				//그룹구분:버튼유무:타이틀:타이틀 옆 색:필요데이터
				var data_order = [
					"DPO:Y:예금/신탁::BNK_PDT_NNM_CNTN|ACNO_FMT||PSTLY_BAMT_AMT_FMT||ACNO|PDT_CD|ACNT_CLACD|",
					"LON:N:대출:blue:BNK_PDT_NNM_CNTN|ACNO_FMT||NOW_LO_BAMT_AMT_FMT|||||",
					"POT:N:포트폴리오:green:ISAMP_PDT_NM||JN_DT_FMT|VLAMT_FMT|PRTF_ENRT_FMT||||"
				];

				for(var i=0; i<data_order.length ; i++)
				{
					var data_order2 = data_order[i].split(":");
					var data_order3 = data_order2[4].split("|");
					
					var LIST_DATA = {};
					LIST_DATA.DATA_LIST = ret_body_['LIST_'+data_order2[0]];	 //계좌정보
					LIST_DATA.TOTAL_MONEY = ret_body_['LIST_'+data_order2[0]+'_AMT_FMT'];	 //전 계좌 잔액 총합
					LIST_DATA.TOTAL_CNT = ret_body_['LIST_'+data_order2[0]+'_CNT'];	 //전 계좌 수
					LIST_DATA.BTN_YN = data_order2[1]; //그룹 내 버튼 유무
					LIST_DATA.TITLE = data_order2[2]; //그룹 타이틀
					LIST_DATA.TITLE_COLOR = data_order2[3]; //그룹 명 앞 구분 색
					
					//SET_DATA = "계좌명|계좌번호|가입일|잔액|수익률|계좌번호2|상품코드|";
					LIST_DATA.SET_DATA = data_order2[4]; //필요데이터 세팅

					LIST_DATA.DATA_LIST2 = new Array(); //계좌정보
					LIST_DATA.DATA_LIST.forEach(function(item,idx)
					{
						var arr_data = new Array();
						for(var j=1; j<=data_order3.length ; j++)
						{
							arr_data['view_data'+j] = item[data_order3[(j-1)]];
						}
						LIST_DATA.DATA_LIST2.push(arr_data);
					});
					
					data[data_order2[0]] = LIST_DATA;
				}
			}
			
			var data_order = "DPO|LON|POT";
			console.log(data_order);
			res.render("views/list"
				,{
					data : data
					,data_order : data_order
				}
			);

			console.log('############## show list');
			//util.log("##### Gerated Access Token=>"+ret.data.data);
		} else {
			res.redirect("/bnk/login");
		}
	})
	.catch((error) => {
		//console.error(error);
		res.redirect("/bnk/login");
	});	
});




/*
//메인화면
router.get("/bnk/main", (req, res) => {
	util.log("메인화면");
	//main 으로 들어오면 바로 페이징 처리
	res.redirect('/bnk/list' );
	//res.redirect('/bnk/paging/' + 1);
});


//-- List
router.get("/bnk/paging/:cur", (req, res) => {

	//--- userinfo
	let cu = util.userData;
	util.log("**** current user => " + cu.username + "," + cu.name + "," + cu.email);
	//-------

	//-- 전체 게시물 수 구한 후 요청된 페이지 표시
	let token = req.cookies[__ACCESS_TOKEN_NAME];
	let _headers = {};
	util.log("request token => " + token);
	_headers[__ACCESS_TOKEN_NAME] = token;
	axios.get(__API_PRODUCT_URI + "/count", {
		headers: _headers
	})
	.then((ret) => {
		util.log("Success to get product count!");
		let data = ret.data;
		let count = (data.success == false ? 0 : data.value);
		renderPage(count);
	})
	.catch((error) => {
		console.error("Fail to get product count", error);
	});

	const renderPage = function (count) {
		const pageSize = 10;		//한 페이지당 문서수
		const pageListSize = 5;		//navaigator bar에 보일 페이지 수
	
		let totalCount = count;			//전체문서수
		if(totalCount < 0) totalCount = 0;

		//-- 전체 문서수에 따른 페이징 control 변수 셋팅
		let curPage = parseInt(req.params.cur);					//현재 페이지
	
		let totalPage = Math.ceil(totalCount / pageSize);		//전체 페이지수
		let totalSet = Math.ceil(totalPage / pageListSize); 	//전체 페이지 세트수
		let curSet = Math.ceil(curPage / pageListSize); 		//현재 셋트 번호
		let startPage = ((curSet - 1) * pageListSize) + 1; 		//현재 세트내 출력될 시작 페이지
		let endPage = (startPage + pageListSize) - 1; 			//현재 세트내 출력될 마지막 페이지
		let startNo = (curPage < 0 ? 0 : (curPage - 1) * pageSize);	//시작 일련 번호 

		let pagingControls = {
			"curPage": curPage,
			"pageListSize": pageListSize,
			"pageSize": pageSize,
			"totalPage": totalPage,
			"totalSet": totalSet,
			"curSet": curSet,
			"startPage": startPage,
			"endPage": endPage,
			"startNo": startNo
		};
		util.log(pagingControls);
		//---------

		//--- 요청된 페이지 데이터 표시
		axios.get(__API_PRODUCT_URI + "/entries", {
			headers: _headers,
			params: {
				order: ["id", "DESC"],
				offset: startNo,
				limit: pageSize
			}
		})
		.then((ret) => {
			let data = ret.data;
			if (data.success) {
				util.log("Success to get entries !");
				res.render("views/main", {
					data: data.value,
					paging: pagingControls,
					user: util.userData
				});
			} else {
				console.error("Fail to get entries", data.msg);
			}
		})
		.catch((error) => {
			console.error("Fail to get entries", error);
		});
	}
});
*/

module.exports = router;
