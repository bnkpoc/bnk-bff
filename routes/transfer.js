/*jshint esversion: 6 */

//---- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
var querystring = require('querystring');
const util = require(__BASEDIR + '/util');
//---------------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//----------

//--- 이체 첫 화면 화면 표시
router.get("/transfer", (req, res) => {
	util.log("이체 첫 화면");

	let token = req.cookies[__ACCESS_TOKEN_NAME];
	let _headers = {};
	util.log("request token => " + token);
	_headers[__ACCESS_TOKEN_NAME] = token;
	_headers['cookie'] = req.cookies['ck-auth'].join('; ');
	_headers['Content-Type'] = 'application/json;charset=UTF-8';
	
	console.log('Cookies: ', req.cookies);
	console.log('token: ', token);
	
	axios.defaults.withCredentials = true;

	//결과데이터
	var data = {};

	//이체 첫 화면
	var url = __TRANSFER_API_URI+'/bnk/transfer1';
	console.log('######### URL: ', url);

	var param = {
		'SECR_MDM_VALID': 'Y',
		'ib20_cur_mnu': 'MWPTWD1000TWD10',
		'TRN_FLAG': 'N',
		'jwt_token': _headers[__ACCESS_TOKEN_NAME],
		'cookie': _headers['cookie']
	};
	
	axios.post(url, 
		param,
		{
			headers: _headers
		}
	)
	.then((ret) => {
		if(ret.status == 200) {

			var ret_common_ = ret.data._msg_._common_;
			var ret_body_ = ret.data._msg_._body_;
			
			data.resCode = ret_common_['resCode'];
			data.customerMessage = ret_common_['error.customer.message'];

			if(ret_common_['resCode']=='00')
			{
				data.transfer_ok_account = ret_body_['DFRY_ACNT_LIST'];

				data.transfer_ok_account.forEach(function(item,idx)
				{
					if(item.DFRY_ACNT_YN == "1") {
						data.account_nm = item.BNK_PDT_NNM_CNTN;
						data.account_no = item.ACNO_FMT;
						data.account_no2 = item.ACNO;
						data.account_ok_money = item.DFRY_PSBL_BAMT_FMT;
						data.account_money = item.PSTLY_BAMT_AMT_FMT;
					}
					else 
					{
						return true;
					}
				});
			}

			res.render("views/transfer"
				,{
					data : data
				}
			);

			//console.log('######### data: ', data);
			
		} else {
			res.redirect("/bnk/login");
		}
	})
	.catch((error) => {
		//console.error(error);
		res.redirect("/bnk/login");
	});	

	console.log('############## show list');
});

module.exports = router;
