/*jshint esversion: 6 */

//---- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
var querystring = require('querystring');
const util = require(__BASEDIR + '/util');
//---------------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//----------

//--- 최근이체현황조회
router.get("/ajax", (req, res) => {
	util.log("최근이체현황조회");

	let token = req.cookies[__ACCESS_TOKEN_NAME];
	let _headers = {};
	util.log("request token => " + token);
	_headers[__ACCESS_TOKEN_NAME] = token;
	_headers['cookie'] = req.cookies['ck-auth'].join('; ');
	_headers['Content-Type'] = 'application/json;charset=UTF-8';
	
	console.log('Cookies: ', req.cookies);
	console.log('token: ', token);
	
	axios.defaults.withCredentials = true;

	//let body = req.body;
	let body = req.query; //ajax로 받을 때 사용


	//console.log("@@@@@@@@@@@@@@@@req : ", req);
	console.log("@@@@@@@@@@@@@@@@body : ", body);

	//결과데이터
	var data = {};
	
	//최근이체현황조회
	var url = __TRANSFER_API_URI+'/bnk/transfer11';
	console.log('######### URL: ', url);

	var param = {
		'SECR_MDM_VALID': 'Y',
		'ib20_cur_mnu': 'MWPTWD1000TWD10',
		'jwt_token': _headers[__ACCESS_TOKEN_NAME],
		'cookie': _headers['cookie']
	};
	
	axios.post(url, 
		param,
		{
			headers: _headers
		}
	)
	.then((ret) => {
		if(ret.status == 200) {

			var ret_common_ = ret.data._msg_._common_;
			var ret_body_ = ret.data._msg_._body_;

			data.resCode = ret_common_['resCode'];
			data.customerMessage = ret_common_['error.customer.message'];

			if(ret_common_['resCode']=='00')
			{
				data.rct_use_data = new Array(); // 최근사용계좌
				data.ftn_use_data = new Array();// 자주사용계좌
				ret_body_['TRN11INQ00000053V00_REC1'].forEach(function(item,idx)
				{
					var arr_data = new Array();
					
					arr_data['account_nm'] = item.DEPS_BANK_NM; //은행명
					arr_data['account_no'] = item.RCNT_RMT_DEPS_ACNO_FMT; //계좌번호1
					arr_data['account_no2'] = item.RCNT_RMT_DEPS_ACNO; //계좌번호2
					arr_data['user_nm'] = item.IBNK_DEPS_ACNT_DPWNM; //계좌주명
					arr_data['bank_cd'] = item.DEPS_ACNT_BNKCD; //은행코드
					arr_data['ftn_use_yn'] = item.FTN_USE_YN; //자주사용 계좌

					data.rct_use_data.push(arr_data);
					if(item.FTN_USE_YN=="Y")
					{
						data.ftn_use_data.push(arr_data);
					}
				});
			}
			
			console.log('######### data: ', data);

			res.render("ajax/transfer_ajax"
				,{
					data : data
				}
			);
			
			//util.log("##### Gerated Access Token=>"+ret.data.data);
		} else {
			res.redirect("/bnk/login");
		}
	})
	.catch((error) => {
		//console.error(error);
		res.redirect("/bnk/login");
	});	
});


//--- 즉시이체/예약이체 예금주조회
router.get("/ajax2", (req, res) => {
	util.log("즉시이체/예약이체 예금주조회");

	let token = req.cookies[__ACCESS_TOKEN_NAME];
	let _headers = {};
	util.log("request token => " + token);
	_headers[__ACCESS_TOKEN_NAME] = token;
	_headers['cookie'] = req.cookies['ck-auth'].join('; ');
	_headers['Content-Type'] = 'application/json;charset=UTF-8';
	
	console.log('Cookies: ', req.cookies);
	console.log('token: ', token);
	
	axios.defaults.withCredentials = true;

	//let body = req.body;
	let body = req.query; //ajax로 받을 때 사용


	//console.log("@@@@@@@@@@@@@@@@req : ", req);
	console.log("@@@@@@@@@@@@@@@@body : ", body);

	//결과데이터
	var data = {};
	
	//최근이체현황조회
	var url = __TRANSFER_API_URI+'/bnk/transfer21';
	console.log('######### URL: ', url);

	var param = {
		'SECR_MDM_VALID': 'Y',
		'ib20_cur_mnu': 'MWPTWD1000TWD10',

		'trnfAmt' : body.in_money, //이체금액
		'depsAcno' : body.in_account_no, //입금계좌
		'depsAcno1' : '', //입금계좌
		'depsAcntBnkCd' : body.in_bank_cd, //입금은행
		'inqTrncnt' : '0', //조회거래건수
		'trnfBooDt' : '', //예약일자
		'inBnkbXprsnCntn' : body.out_msg, //내통장표시내용
		'rcvrBnkbXprsnCntn' : body.in_msg, //받는분통장표시
		'cmsOrgtCd' : '', //CMS코드
		'depsAcntBnkCd1' : '', //입금은행 (입금계좌지정)
		'ACNT_PSWD' : '', //출금계좌비밀번호
		'E2E_ACNT_PSWD' : '', //출금계좌비밀번호 E2E
		'holidayCheckYn' : '', //휴일거래 검증여부
		'dfryBnkcd' : '032', //출금은행 
		'acntInputFrml' : 'T', //계좌선택방식
		'dfryAcno2' : body.out_account_no, //출금계좌번호
		
		'jwt_token': _headers[__ACCESS_TOKEN_NAME],
		'cookie': _headers['cookie']
	};

	
	axios.post(url, 
		param,
		{
			headers: _headers
		}
	)
	.then((ret) => {
		if(ret.status == 200) {

			var ret_common_ = ret.data._msg_._common_;
			var ret_body_ = ret.data._msg_._body_;
			
			data.resCode = ret_common_['resCode'];
			data.customerMessage = ret_common_['error.customer.message'];

			if(ret_common_['resCode']=='00')
			{
				data.out_account_no = ret_body_.DFRY_ACNO_FMT;//출금계좌번호
				data.out_account_no2 = ret_body_.DFRY_ACNO;//출금계좌번호2
				data.out_bank_cd = ret_body_.DFRY_BNKCD;//출금은행코드
				data.out_user_nm = ret_body_.DFRY_ACNT_CUST_NM;//출금계좌고객명
				data.in_account_no = ret_body_.DEPS_ACNO_FMT;//입금계좌번호
				data.in_account_no2 = ret_body_.DEPS_ACNO;//입금계좌번호2
				data.in_bank_nm = ret_body_.DEPS_BANK_NM; //입금은행명
				data.in_bank_cd = ret_body_.DEPS_BNKCD;//입금은행코드
				data.in_money = ret_body_.TRNF_AMT;// 거래금액
				data.in_money2 = ret_body_.TRNF_AMT_FMT;// 거래금액2
				data.in_fee = ret_body_.RMT_FE_SMAMT;// 송금수수료합계금액
				data.in_user_nm = ret_body_.DEPS_ACNT_CUST_NM;//입금계좌고객명
				data.my_account_yn = ret_body_.HSLF_ACNT_YN;//본인계좌여부
				data.out_account_memo = ret_body_.IN_BNKB_XPRSN_CNTN;//출금통장표시
				data.in_account_memo = ret_body_.RCVR_BNKB_XPRSN_CNTN;//입금통장표시
				data.gm_no = ret_body_.TRNS_GMNO;//입금통장표시
			}

			res.render("ajax/transfer_ajax2"
				,{
					data : data
				}
			);
		} else {
			console.error("######### error1 : ", error);
			res.redirect("/bnk/login");
		}
	})
	.catch((error) => {
		console.error("######### error2 : ", error);
		res.redirect("/bnk/login");
	});	
});



//--- 이체입력확인
router.get("/ajax3", (req, res) => {
	util.log("이체입력확인");

	let token = req.cookies[__ACCESS_TOKEN_NAME];
	let _headers = {};
	util.log("request token => " + token);
	_headers[__ACCESS_TOKEN_NAME] = token;
	_headers['cookie'] = req.cookies['ck-auth'].join('; ');
	_headers['Content-Type'] = 'application/json;charset=UTF-8';
	
	console.log('Cookies: ', req.cookies);
	console.log('token: ', token);
	
	axios.defaults.withCredentials = true;

	//let body = req.body;
	let body = req.query; //ajax로 받을 때 사용


	//console.log("@@@@@@@@@@@@@@@@req : ", req);
	console.log("@@@@@@@@@@@@@@@@body : ", body);

	//결과데이터
	var data = {};
	
	//이체입력확인
	var url = __TRANSFER_API_URI+'/bnk/transfer22';
	console.log('######### URL: ', url);

	var transfer_list = [{
		"BANK_NM"	:	body.in_bank_nm, //은행명
		"DEPS_ACNO_FMT"	:	body.in_account_no, //입금계좌번호
		"DFRY_ACNO_FMT"	:	body.out_account_no, //출금계좌번호
		"SendMsg_after"	:	"",
		"acntInputFrml_Type"	:	"lastGJ",
		"acntInputFrml_after"	:	"Y",
		"cmsOrgtcd_after"	:	"",
		"depsAcno_after"	:	body.in_account_no2, //입금계좌번호
		"depsAcntCustNm_after"	:	body.in_user_nm, //입금계좌예금주명
		"depsBnkCd_after"	:	body.in_bank_cd, //입금은행코드
		"dfryAcno_after"	:	body.out_account_no2, //출금계좌번호
		"dfryAcntCustNm_after"	:	body.out_user_nm, //출금계좌예금주명
		"dfryBnkCd_after"	:	body.out_bank_cd, //출금은행코드
		"fexmpCntn_after"	:	"",
		"hslfAcntYn_after"	:	body.my_account_yn, //본인계좌여부
		"inBnkbAcntMemoCntn_after"	:	"",
		"inBnkbXprsnCntn_after"	:	body.out_account_memo, //내통장표시
		"rcvrBnkbMemoCntn_after"	:	"",
		"rcvrBnkbXprsnCntn_after"	:	body.in_account_memo, //받는분통장표시
		"rcvtlno_after"	:	"",
		"rmtFeSmamt_after"	:	body.in_fee, //송금수수료
		"rmtpstlno_after"	:	"",
		"simpleAcnoYn_after"	:	"",
		"trnfAmt_after"	:	body.in_money, //이체금액
		"trnfBooDt_after"	:	"",
		"trnfBooHr_after"	:	"",
		"trnfTrnsDualYn_after"	:	"0", //이체거래이중여부
		"trnsGmno_after"	:	body.gm_no, //전문번호
		"trnsNatvNo_after"	:	""
	}];

	var param = {
		'SECR_MDM_VALID': 'Y',
		'ib20_cur_mnu': 'MWPTWD1000TWD10',

		'wcDd1TrnfAcAmt': '0',
		'TRANSFER_LIST' : JSON.stringify(transfer_list), //출금정보
		
		'jwt_token': _headers[__ACCESS_TOKEN_NAME],
		'cookie': _headers['cookie']
	};

	
	axios.post(url, 
		param,
		{
			headers: _headers
		}
	)
	.then((ret) => {
		if(ret.status == 200) {
			var ret_common_ = ret.data._msg_._common_;
			var ret_body_ = ret.data._msg_._body_;

			data.resCode = ret_common_['resCode'];
			data.customerMessage = ret_common_['error.customer.message'];

			if(ret_common_['resCode']=='00')
			{
				console.log("###### ret_body_", ret_body_);

				data.transfer_res_data = new Array();
				ret_body_.MBPTWD110000A00P_REC1.forEach(function(item,index){
					var arr_data = new Array();

					arr_data['out_account_no'] = item.DFRY_ACNO_FMT; //출금계좌번호
					arr_data['out_account_no2'] = item.DFRY_ACNO; //출금계좌번호
					arr_data['out_bank_cd'] = item.DFRY_BNKCD; //출금계좌번호
					arr_data['out_user_nm'] = item.DFRY_ACNT_CUST_NM; //출금계좌번호
					arr_data['out_account_memo'] = item.IN_BNKB_XPRSN_CNTN; //입금계좌번호

					arr_data['in_account_no'] = item.DEPS_ACNO_FMT; //입금계좌번호
					arr_data['in_account_no2'] = item.DEPS_ACNO; //입금계좌번호
					arr_data['in_bank_cd'] = item.DESP_BNKCD; //입금계좌번호
					arr_data['in_user_nm'] = item.DEPS_ACNT_CUST_NM; //입금계좌번호
					arr_data['in_account_memo'] = item.RCVR_BNKB_XPRSN_CNTN; //입금계좌번호
					
					arr_data['in_money'] = item.TRNF_AMT_FMT; //입금계좌번호
					arr_data['in_money2'] = item.TRNF_AMT; //입금계좌번호
					arr_data['in_fee'] = item.RMT_FE_SMAMT_FMT; //입금계좌번호
					arr_data['in_fee2'] = item.RMT_FE_SMAMT; //입금계좌번호
					
					arr_data['gm_cd'] = item.TRNS_GMNO; //입금계좌번호
					arr_data['my_account_yn'] = item.HSLF_ACNT_YN; //입금계좌번호
					
					data.transfer_res_data.push(arr_data);
				});
			}
			
			res.render("ajax/transfer_ajax3"
				,{
					data : data
				}
			);
			
			//util.log("##### Gerated Access Token=>"+ret.data.data);
		} else {
			console.error("######### error1 : ", error);
			res.redirect("/bnk/login");
		}
	})
	.catch((error) => {
		console.error("######### error2 : ", error);
		res.redirect("/bnk/login");
	});	
});

module.exports = router;
