const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

global.__BASEDIR = __dirname + '/';

const port = (process.env.PORT || 8090);

const app = express();
app.use(express.static(path.join(__BASEDIR, '/public')));		//static resource 폴더 
app.use(bodyParser.urlencoded({extended:false}));				//include request 객체 parser


//----- middle ware: routing되는 서버모듈 시작 전에 항상 수행-인증토큰 검증
app.use(function(req, res, next) {
    res.writeHead(200, { 'Content-Type':'text/html; charset=utf-8' });
    res.write('I am alive');
    res.end();
});

//----- start web server 
app.listen(port, () => {
	console.log('Listen: ' + port);
});

