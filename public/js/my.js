$(document).ready(function(){
    
    $(".money_info").on("keyup",function(){
        var this_val = $(this).val();
        this_val = this_val.replace(/\,/g,"");
        var this_val2 = numberWithCommas(this_val);
        //alert(this_val2);
        $(this).val(this_val2);
    });
});


var viewKorean = function(num) { 
    var hanA = new Array("","일","이","삼","사","오","육","칠","팔","구","십"); 
    var danA = new Array("","십","백","천","","십","백","천","","십","백","천","","십","백","천"); 
    var result = ""; 
    num = num.replace(/\,/g, '');
    
    for(i=0; i<num.length; i++) 
    { 
        str = ""; 
        han = hanA[num.charAt(num.length-(i+1))]; 

        if(han != "") 
        {
            str += (danA[i]=="천" ? " " : "")+han+danA[i]; 
        }
        if(i == 4) str += "만"; 
        if(i == 8) str += "억"; 
        if(i == 12) str += "조"; 
        result = str + result; 
    } 

    if(num != 0) result = result + "원"; 
    return result ; 
}


var numberWithCommas = function(inputNumber) {
    return inputNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
