// Common _ footer bar / top btn
$.fn.scrollEnd = function (callback, timeout) {
    $(this).scroll(function () {
        var $this = $(this);
        if ($this.data('scrollTimeout')) {
            clearTimeout($this.data('scrollTimeout'));
        }
        $this.data('scrollTimeout', setTimeout(callback, timeout));
    });
};

$(window).scrollEnd(function () {
    if ($('.btn-pagetop').length > 0) {
        if ($(this).scrollTop() > 50) {
            $('.btn-pagetop').addClass('active');
        } else {
            $('.btn-pagetop').removeClass('active');
        }

        $('.btn-pagetop').click(function () {
            var pTop = $($(this).attr('href')).offset().top;
            $('html, body').animate({ scrollTop: pTop }, 200);
            return false;
        });

        direction = "";
    }
}, 300);

var didScroll;
var direction = "";
$(window).scroll(function () {

    $('.btn-pagetop').removeClass('active');

    var i = navigator.userAgent;

    if (/Chrome/i.test(i) || /CriOS/i.test(i)) {

    } else if (/Safari/i.test(i)) {
        if ((document.documentElement.clientHeight + 114) >= document.body.scrollHeight - $(this).scrollTop()) {
            return;
        }
    }
    if ($(this).scrollTop() < didScroll || $(this).scrollTop() <= 0) {
        if (direction != "UP") {
            $('.footer-bar').css({
                bottom: '0px',
                transition: 'bottom ease-in-out 0.5s'
            });
        }
        direction = "UP";
    } else {
        if (direction != "DOWN") {
            $('.footer-bar').css({
                bottom: '-70px',
                transition: 'bottom ease-in-out 0.5s'
            });
        }
        direction = "DOWN";
    }
    didScroll = $(this).scrollTop();

});

$(document).ready(function(){
    // dim 클릭시 
    $('.dim').on('click',function(){
        console.log(' DIM CLICK ');
        $('.popup-wrap').removeClass('active');
    });
    // 박스 넓이 설정
    $('.ctr-radio .box').each(function(){
        var boxW = parseFloat(100 / $(this).parent().find('li').length)+'%';
        $(this).css('width', boxW);
    })
    // 박스 움직임 설정
    $('.ctr-radio li input').on('click',function() {
        var moveLeft = $(this).offset().left - 21;
        $(this).parent().parent().next('.box').css('left', moveLeft);
    });
    // 뒤로가기 버튼
    $('.btn-back').on('click',function(){
        history.back();
    });
    // 계좌 즐겨찾기 버튼
    $('.btn-favorit').on('click',function(){
        $(this).toggleClass('active');
    })
    // 달력 팝업 닫기 버튼
    $('.date-picker .btn-close').on('click',function(){
        $('.date-picker').hide();
    });
    $('#myAccList .list-type1 li a').on('click',function(){
        $('#myAccList .list-type1 li').removeClass('active');
        $(this).parent().addClass('active');

        closePopup('myAccList');

        if( $(this).attr('href') == '#' ){
            var accNum = $(this).find('strong').text();
            var accName = $(this).find('p').first().text();

            $('.card .info > .top span').text(accName);
            $('.card .info > .top a').text(accNum);
        }
    });

    // Tab 작동
    $('.tab-wrap button').on('click',function(){
        var $tabWrap = $(this).parent().parent();
        var tabDataNum = $(this).parent().parent().data('tab');
        var idx = $(this).parent().parent().find('button').index($(this));
        
        $tabWrap.find('li').removeClass('active').eq(idx).addClass('active');
        $('.tab-con-wrap[data-tab="' + tabDataNum + '"]').find('> .tab-con').removeClass('active').eq(idx).addClass('active');
    });

    //input 삭제 버튼 표현
    $('.inp input').on('propertychange change keyup paste input',function(){
        if( $(this).prop('value') != "" ){
            $(this).css('padding-right', '30px').next().css('opacity', 1);
        }else{
            $(this).css('padding-right', '0').next().css('opacity', 0);
        }
    });
    $('.inp input').on('focusout',function(){
        $(this).css('padding-right', '0').next().css('opacity', 0);
    });
    $('.inp .btn-delete').on('click',function(){
        $(this).css('opacity','0');
        $(this).prev().css('padding-right','0').prop('value','');
    });
    
    $('.inp-txt-wrap .inp input').on('focus',function(){
        $(this).parent().parent().parent().addClass('active focus');
    });
    $('.inp-txt-wrap .inp input').on('focusout',function(){
        $(this).parent().parent().parent().removeClass('focus');
    });

    // 금액
    $('.only-num').bind('keyup keydown', function () {
        inputNumberFormat(this);
    });
});




// LayerPopup
function openPopup(id){
    $('#' + id).addClass('active');
    $('footer').before('<div class="dim"></div>');
    return false;
}
function closePopup(id){
    $('#'+id).removeClass('active');
    $('.dim').remove();
    return false;
}


// LayerPopup
function openLoading(){
    //$('footer').before('<div class="dim"></div>');
    $('footer').before('<div class="loading-div"><img class="loading-img" src="../img/loading@3x.gif" /></div>');
    return false;
}
function closeLoading(){
    //$('.dim').remove();
    $('.loading-div').remove();
    return false;
}

// toast MSG
/* 2초 뒤 사라지도록 되어있음
 * _tit : toast 메세지
 * _type : ico-alert, ico-confirm, ico-query, innder-dl 타입 중 하나, 쓰지않은면 default
 * _desc : innder-dl일때 내용 
 */
function toastMSG(_tit, _type , _desc){
    if (_type == undefined) _type = '';

    var html = '';
        html += '<div class="msg-toast ' + _type + '">';
    if (_type != 'inner-dl' ){
        html += '   <p>' + _tit + '</p>';
    }else{
        html += '   <dl>';
        html += '       <dt>' + _tit + '</dt>';
        html += '       <dd>' + _desc + '</dd>';
        html += '   </dl>';
    }
        html += '</div>';

    $('footer').after(html);

    setTimeout(function(){ $('.msg-toast').addClass('active'); },300);
    setTimeout(function(){ $('.msg-toast').removeClass('active').delay(250).empty().remove(); },2300);
}

// 조회 조건
function searchComplete(){
    var srchTxtArray = new Array;
    // 체크된 값 배열입력
    $('#searchCondition input').each(function(){
        if( $(this).prop('checked') == true) srchTxtArray.push($(this).next('label').text());
    });
    closePopup('searchCondition');
    // 결과값 전달(달력 제외)
    for (i = 0; i < $('.sort span').length ; i++) {
        $('#sortCate' + (i + 1)).text(srchTxtArray[i]);
    }
    var calStartDay = $('#calendarStart').prop('value');
    var calEndDay = $('#calendarEnd').prop('value');
    $('.sort-wrap .search-date').text(calStartDay + '~' + calEndDay);
}
//입력한 문자열 전달
function inputNumberFormat(obj) {
    obj.value = comma(uncomma(obj.value));
    if ($('.only-num-kr').length > 0){
        $('.only-num-kr').text(viewKorean(uncomma(obj.value)));
    }
}
//콤마찍기
function comma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}
//콤마풀기
function uncomma(str) {
    str = String(str);
    return str.replace(/[^\d]+/g, '');
}
//숫자만 리턴(저장할때)
function cf_getNumberOnly(str) {
    var len = str.length;
    var sReturn = "";

    for (var i = 0; i < len; i++) {
        if ((str.charAt(i) >= "0") && (str.charAt(i) <= "9")) {
            sReturn += str.charAt(i);
        }
    }
    return sReturn;
}
function viewKorean(num) {	
    var hanA = new Array("","일","이","삼","사","오","육","칠","팔","구","십");
    var danA = new Array("","십","백","천","","십","백","천","","십","백","천","","십","백","천");
    var result = "";
	for(i=0; i<num.length; i++) {		
		str = "";
		han = hanA[num.charAt(num.length-(i+1))];
		if(han != "")
			str += han+danA[i];
		if(i == 4) str += "만";
		if(i == 8) str += "억";
		if(i == 12) str += "조";
		result = str + result;
	}
	if(num != 0)
        result = result + "원";

    return result ;
}